const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const connectionString = "mongodb+srv://aris:<password>@cluster0.e1e4p.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: true }));



MongoClient.connect(connectionString, { useNewUrlParser: true })
    .then(client => {
        console.log('Connected to Database')
        const db = client.db('zettadb')
        const commentsCollection = db.collection('comments')
        app.post('/comments', (req, res) => {
            commentsCollection.insertOne(req.body)
                .then(result => {
                    console.log(result)
                })
                .catch(error => console.log(error))
        })

        app.get('/', (req, res) => {
            
            db.collection('comments').find().toArray()
                .then(results => {
                    res.render('index.ejs', { comments: results })  
                })
                .catch(error => console.error(error))
            
        })
    })
    .catch(err => console.log(err))
    

