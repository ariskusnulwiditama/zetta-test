/**
 * Direction:
 * Find all fields that have different value & must can detect all field dynamically
 *
 * Expected Result:
 * ['firstName', 'lastName']
 *
 */
 const datas = [
    { firstName: 'Adi', lastName: 'Nugroho', age: 25 },
    { firstName: 'Deddy', lastName: 'Dores', age: 25 },
  ];
  
//   Expected Result:['firstName', 'lastName'] no age
  function result(data) {
    let newData = [];
    for (let i = 0; i < data.length; i++) {
        for (let key in data[i]) {
            if (newData.indexOf(key) === -1) {
            //    not show age
                if (key !== 'age') {
                    newData.push(key);
                }
            }
        }
    }
    return newData;
  }
  
  console.log(result(datas));