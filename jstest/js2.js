// Question
// Given a object data, return the data multiple by 3 and sort the data.
// Expected output : { j: 0, k: 9, i: 18, l: 36 }

const data = { i: 6, j: null, k: 3, l: 12 };

function result(data) {
  
    let newData = {};
   
    for (let key in data) {
        if (data[key] !== null) {
            newData[key] = data[key] * 3;
        }
    }
    // sort newData
    let sortedData = Object.keys(newData).sort((a, b) => newData[a] - newData[b]);
   
    return newData;
}
console.log(result(data));